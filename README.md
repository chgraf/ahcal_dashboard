AHCAL Dashboard
===============

Author: Christian Graf
Date: August 2020

Overview
--------

Dashboard for easily comparing key distributions between testbeam data and simulated data
in order to quickly verify a data set.

Aim: The dashboard should be easy to use without installation or expert knowledge

Problems: Where to deploy the server? Security problems hosting it on DESY servers, data protection
problems hosting it somewhere else. Possible solution: accissible via secure connection on desy
servers.

Packages 
--------

* Relying on python 3.6. 
* Using Holoviz ecosystem for the dashboard
* Panel for interactivity
* matplotlib / seaborn for plotting
* pandas as a datastructure
* uproot for fast reading of root files into pandas dataframes
